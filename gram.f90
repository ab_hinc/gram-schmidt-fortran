program gram
	! Gram Schmidt function main program
	use functions, only: normalization, component
	
	implicit none
	integer, parameter :: n = 3
	real(kind=8) :: curvec(n), tempvec1(n), tempvec2(n)
	real(kind=8), dimension(n,n) :: arrayofvec, temparr, finalset
	! Local Variables
	integer :: i, j
	
	! Array containing vectors in question
	arrayofvec = reshape((/1.d0, 2.d0, 3.d0, 4.d0, 5.d0, 6.d0, 7.d0, 8.d0, 9.d0/),(/n,n/))
	
	call normalization(arrayofvec(:,1),finalset(:,1),n)
	temparr(:,:) = 0.d0
	do i=2,n
		tempvec2(:)=0.d0
		do j=1,i-1
			tempvec1(:)=0.d0
			call component(finalset(:,j),arrayofvec(:,i),tempvec1,n)
			temparr(:,i) = temparr(:,i) + tempvec1
		enddo
		tempvec2 = arrayofvec(:,i) - temparr(:,i)
		call normalization(tempvec2,finalset(:,i),n)
	enddo
	do i=1,n
		print *,finalset(:,i)
	enddo
end program gram