# README #

**Gram Schmidt Procedure**
* Its is used to generate orthonormal basis vectors from randomly oriented vectors. This has ever present uses, ranging from Quantum Mechanics to General Relativity.
* This version of the code exploits Arrays to calculate the orthonormal basis for vectors composed of real numbers.

Set-up

* The subroutines are in place along with the code to run it. Make file takes care of automating the procedure.
* The values of the vectors need to be input within the gram.f90 file, in succession on line 13. Say the vector is (1,2,4), then the values within the first bracket in line 13 should be changed to 1.d0, 2.d0, 3.d0. The values of successive vectors need to be added in addition
* Finally change the dimension by changing the value of n on line 6
* Can be run with both make file or using Fortran.

**Owner : Karthik Mohan | **
**Email: karthik.mohan390@gmail.com**