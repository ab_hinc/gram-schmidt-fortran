OBJECTS = functions.o gram.o
MODULES = functions.mod

.PHONY: test clean

result.txt: gram.exe
		./gram.exe > result.txt

gram.exe: $(MODULES) $(OBJECTS)
		gfortran $(OBJECTS) -o gram.exe

%.o:%.f90
		gfortran -c $<

%.mod:%.f90
		gfortran -c $<

clean:
		rm -f *.o *.exe *.mod