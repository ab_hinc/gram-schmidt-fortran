module functions

contains

	subroutine normalization(x,y,n)
		! A function to normalize the vectors
		! x represents the input vector
		! n represents the vector's dimension
		implicit none
		integer, intent(in) :: n
		real(kind=8), intent(in) :: x(n)
		real(kind=8), intent(out) :: y(n)
		! Local Variables
		real(kind=8) :: i
		do i=1,n
			y(i) = x(i)/(sqrt(dot_product(x,x)))
		enddo
	end subroutine normalization
	
	subroutine component(x,y,z,n)
	!A function to calculate the component of y in x
	!n is the dimension
	!z is the output
	implicit none
	integer, intent(in) :: n
	real(kind=8), intent(in) :: x(n), y(n)
	real(kind=8), intent(out) :: z(n)
	!Local variables
	integer :: i
	do i=1,n
		z(i) = dot_product(x,y)*x(i)
		enddo
	end subroutine component
	
end module functions